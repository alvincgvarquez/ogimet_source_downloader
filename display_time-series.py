from netCDF4 import Dataset
import os
import itertools
import numpy
from scipy import stats
import matplotlib.pyplot as plt
import time
from datetime import timedelta,datetime
import glob
import pandas as pd
from matplotlib import dates as d
import matplotlib

start = time.time()

def time_series_comparison(wrf_files,obs_file,focused_lon,focused_lat,ylow,yhigh,variable,pstart,pend):
    file_list = sorted(glob.glob(wrf_files))
    fh    = Dataset(file_list[0],mode='r')
    lons  = fh.variables['XLONG'][0,0,:]
    lats  = fh.variables['XLAT'][0,:,0]
    ttime = fh.variables['Times'][0,:]
    i     = find_closest(lats,focused_lat)
    j     = find_closest(lons,focused_lon)
    UTC = +7

    xtime1 = numpy.array([])
    yval1  = numpy.array([])

    for ifil in file_list:
          fh    = Dataset(ifil,mode='r')
          ttime = fh.variables['Times'][0,:]
          t2    = fh.variables[variable][0,i,j]-273.15
          loctime = datetime(int(''.join(ttime[0:4])),int(''.join(ttime[5:7])),int(''.join(ttime[8:10])),int(''.join(ttime[11:13])))+timedelta(hours=UTC)
          xtime1 = numpy.append(xtime1,loctime)
          yval1  = numpy.append(yval1,t2)
          del fh

    simulation = pd.Series(yval1,index=xtime1)
    data = pd.DataFrame({'WRF':simulation})
    ax = data.plot(figsize=(15,5))

    dateparse = lambda x: pd.datetime.strptime(x,'%Y%m%d%H%M')
    df = pd.read_csv(obs_file,na_values=['*','**','***','****','*****','******'],sep='\s+',index_col='datetime',skip_blank_lines=True,parse_dates={'datetime':[2]},date_parser=dateparse,low_memory=False)
    df.index = df.index + timedelta(hours=UTC)
    ax2 = df['TEMPC'].plot(ax=ax,style='r.')
    ax2.set_xlim(pd.Timestamp(pstart),pd.Timestamp(pend))
    ax2.set_ylim(ylow,yhigh)
    plt.grid(b=True, which='major', color='grey', linestyle='-')
    plt.savefig(obs_file.replace(".txt","")+'_2015-08_timeseries.png')
    plt.clf()

def find_closest(A, target):
    #A must be sorted
    idx = A.searchsorted(target)
    idx = numpy.clip(idx, 1, len(A)-1)
    left = A[idx-1]
    right = A[idx]
    idx -= target - left < right - target
    return idx

def main():
    simdir = './wrfout*d03*-08*' #List of simulation
    obsfile = '967510-99999.txt' #Filename of observation
    obslon = 106.933             #Longitude of observation
    obslat = -6.7                #Latitude of observation
    ylow   = 12.                 #Y-axis lower limit
    yhigh  = 30.                 #Y-axis upper limit
    variable = 'T_200ZR_URB2D'   #Simulatin variable for comparison with observation.
    pstart = '2015-08-01'        #Period start for X-axis
    pend   = '2015-08-30'        #Period end for X-axis
    time_series_comparison(simdir,obsfile,obslon,obslat,ylow,yhigh,variable,pstart,pend)
    end = time.time()
    elapsed = end - start
    print "Completed at duration:",elapsed

main()
