#!/bin/bash
# Don't forget to type "bash launch.bash" first.
# Prepared by Varquez 2017028 varquez.a.aa@m.titech.ac.jp

filelist=( 486940-99999 486980-99999 )
year=( 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 )
for fname in ${filelist[@]}
do
 for yr in ${year[@]}
 do
  echo "Downloading: "${fname}-${yr}
  wget --no-check-certificate https://www1.ncdc.noaa.gov/pub/data/noaa/${yr}/${fname}-${yr}.gz
  gunzip ${fname}-${yr}.gz
  java -classpath . ishJava_v2 ${fname}-${yr} ${fname}-${yr}.out
  rm ${fname}-${yr}
 done
  cat ${fname}-${year[0]}.out | grep -i "WBAN" > header.txt
  cat ${fname}*.out | grep -v "WBAN" > ${fname}_headerless.txt
  cat header.txt ${fname}_headerless.txt >  ${fname}.txt
  rm *.out *_headerless.txt
done
