#!/bin/bash
# Don't forget to acquire internet access.
# Prepared by Varquez 20170307 varquez.a.aa@m.titech.ac.jp
# Base files from NOAA used by OGIMET

export PATH=/work1/t2gwrf-group/share/python/python-2.7.13/bin:$PATH
export PYTHONPATH=/work1/t2gwrf-group/share/python/python-2.7.13:/work1/t2gwrf-group/share/python/python-2.7.13/lib
export PYTHONHOME=/work1/t2gwrf-group/share/python/python-2.7.13
export GEOS_DIR=/work1/t2gwrf-group/share/geos-3.3.3
echo "You know have the most powerful python in Tsubame 2.0 - Alvin"

wget -check-certificate https://www1.ncdc.noaa.gov/pub/data/noaa/country-list.txt
wget --no-check-certificate https://www1.ncdc.noaa.gov/pub/data/noaa/isd-history.txt
wget --no-check-certificate https://www1.ncdc.noaa.gov/pub/data/noaa/isd-inventory.txt
wget --no-check-certificate https://www1.ncdc.noaa.gov/pub/data/noaa/ish-abbreviated.txt
wget --no-check-certificate https://www1.ncdc.noaa.gov/pub/data/noaa/ishJava.class
wget --no-check-certificate https://www1.ncdc.noaa.gov/pub/data/noaa/ishJava.java
wget --no-check-certificate https://www1.ncdc.noaa.gov/pub/data/noaa/ishJava_ReadMe.pdf

javac ishJava_v2.java
