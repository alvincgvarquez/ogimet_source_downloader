![Kanda Lab Logo](http://www.ide.titech.ac.jp/~kandalab/logo_kanda_20171.gif) 
# OGIMET Source Data Downloader
prepared by Alvin C.G. Varquez (varquez.a.aa[at]m.titech.ac.jp)

## 1. Introduction ##

The program is a downloader and parser of weather stations. Refer to ~/source_info folder to identify the ID of the desired stations.
Inspect each text file by yourself. The text files are self-explanatory.

## 2. Installation ##
1. Download all files and upload into a work folder in Tsubame. To download, click 'Downloads' at the NAVIGATION panel.
2. Enter the folder and key-in:
```
#!bash
bash install.bash
```

## 3. Downloading ##
1. Access the internet by keying-in:
```
#!bash
bash launch.bash
```
2. cd to your install directory.
3. Edit download.bash. Replace "filelist" and "year" entries. Make sure formatting is preserved. Save the script.
4. Refer to the file "isd-history.txt" to know the files you need to download for the "filelist".
5. Type:
```
#!bash
bash download.bash
```
5. *.txt files will be created containing the data.

# Bonus Tool #
Processing the downloading text files and plotting it with the WRF results using the program "display_time-series.py"

1. In the program, edit the following (right side of '=') using any text editor of choice:

```
#!python
    simdir = './wrfout*d03*-08*' #List of simulation including directory
    obsfile = '967510-99999.txt' #Filename of observation
    obslon = 106.933             #Longitude of observation
    obslat = -6.7                #Latitude of observation
    ylow   = 12.                 #Y-axis lower limit
    yhigh  = 30.                 #Y-axis upper limit
    variable = 'T_200ZR_URB2D'   #Simulatin variable for comparison with observation.
    pstart = '2015-08-01'        #Period start for X-axis
    pend   = '2015-08-30'        #Period end for X-axis
```

2. Run the program by typing:

```
#!python

python display_time-series.py
```
